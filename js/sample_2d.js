(function(exports){

var BaseScene2D = enchant.Class.create({
	initialize: function(parentId) {
		this._init(parentId);
		this._initEvent();
	},
	_init: function(parentId) {
		this._stage = new PIXI.Container();
		
		this._renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight);

        document.getElementById(parentId).appendChild(this._renderer.view);
	},
	_initEvent: function() {
		this._resize();
		window.addEventListener('resize', this._resize.bind(this));
	},
	_resize: function() {
		var width = window.innerWidth;
		var height = window.innerHeight;
		this._renderer.resize(window.innerWidth, window.innerHeight);
	},
	start: function() {
		var self = this;
		var animate = function(){
	    	requestAnimationFrame(animate);
		    TWEEN.update();
		    self._renderer.render(self._stage);
		}
		animate();
	},
	addChild: function(child) {
		this._stage.addChild(child);
	}
});

var SampleObject1 = enchant.Class.create(PIXI.Container, {
	initialize: function(scene) {
		PIXI.Container.call(this);
		scene.addChild(this);
		this._init();
	},
	_init: function() {
		var word = "Hello World!";
		var style = {font:'bold 60pt Arial', fill:'white'};
		var textobj = new PIXI.Text(word, style);
		textobj.position.x = 0;
		textobj.position.y = 0;
		this.addChild(textobj);
	},
	start: function() {
		var _t1 = new Date().getTime();
		var tw = new TWEEN.Tween(this.position).to({x: 300}, 2000);
		tw.start();
	}
});


exports.initApp = function() {

	var scene = new BaseScene2D('main-canvas-container');
	scene.start();

	var sample1 = new SampleObject1(scene);
	sample1.start();

};

})(window);